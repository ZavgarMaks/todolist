/**
 * Created by dunice on 07.04.17.
 */
'use strict';


var todoList = [];
var mode = 'all';
var currentPage = 1;
var todoOnPage = 5;
var $list = $('#list');
var $footer =  $('.footer')

$(document).ready(()=> {
    print(todoList);

    $('#addNewTodo').submit(pushInArray);
    $('#checkAll').on('change', checkAll);
    $list.on('change','.checkboxes', checkedOne );
    $list.on('click','.remove', removeOne);
    $list.on('dblclick','.undertext', startEdit);
    $list.on('blur','.undertext',finishEdit);
    $footer.on('click','#showAll',showAll);
    $footer.on('click','#showOnlyDone',showOnlyDone);
    $footer.on('click','#showOnlyNotDone',showOnlyNotDone);
    $footer.on('click','#delete-done',deleteCompleted);
    $('#paginPanel').on('click','input', goToCurrentPage);
});

function pushInArray (e) {
    var itemWithMaxID = _.max(todoList, function (item) {return item.id;});
    var maxID=itemWithMaxID.id || 0;
    console.log('this', this, e.currentTarget);
    e.preventDefault();
    var textTodo = document.getElementById("nameTodo").value.trim();

    todoList.push({id: ++maxID, done: false, text: textTodo});
    print(todoList);

    $('#nameTodo').val('');
}

function checkAll (e) {
    var checkedAllorNotCheckedAll = this.checked;
    todoList.forEach(function (item) {
        item.done =  checkedAllorNotCheckedAll;
    });
    print(todoList);

}

function checkedOne (e) {
    var idCheck=($(this).parent()[0].id);
    todoList.forEach(function (item) {
        if(item.id == idCheck) {
            item.done = !item.done;
        }
    });
    print(todoList);
}

function removeOne (e) {
    var idRemove=($(this).parent()[0].id);
    todoList.forEach((item,i)=> {
        if (item.id==idRemove) {todoList.splice(i,1)}
    });
    print(todoList);
}

function startEdit (e) {
    $(this).prop('readonly', false);
}

function finishEdit () {
    var idEdit = ($(this).parent()[0].id);
    $(this).prop('readonly', true);
    var editText = $(this)[0].value.trim();
    todoList.forEach(function (item,i) {
        if(item.id == idEdit) {
            if(editText) return item.text = editText;
            todoList.splice(i,1);
        }
    });
    print(todoList);
}

function goToCurrentPage () {
    currentPage = $(this)[0].className;
    print(todoList);
}
function print(todoList) {

    var stringToAppend='';
    var filteredListByMode = filterByMode(todoList);
    console.log('-->');
    var amountPages = Math.ceil(filteredListByMode.length/todoOnPage);

    if (!filteredListByMode.length) {
        currentPage=1;
        var  slicedList=[];
    } else{

       slicedList = filteredListByMode.slice((currentPage-1) * todoOnPage,currentPage * todoOnPage );

        if (slicedList.length==0) {
            if (currentPage!=1 ) {
                currentPage = amountPages;
                slicedList=filteredListByMode.slice((currentPage-1) * todoOnPage,currentPage * todoOnPage );
            }
        }

    }
    slicedList.forEach((item)=>{

        stringToAppend +=`
        <div id=${item.id} class="oneTodo"><input type="checkbox" class="checkboxes" ${item.done?'checked':''}/>
        <input type="text" class="undertext" readonly="true" value="${item.text}"><button class="remove"></button> </div>
        `;
        console.log('i',item)
    });

    $list.empty();
    $list.append(stringToAppend);

    PaginPanelAndIndicator(slicedList,amountPages);
}
function PaginPanelAndIndicator(slicedList,amountPages) {
    var stringPagin = "";
    if (slicedList.length !==0){
        for (var i = 1; i <= amountPages; i++) {
            stringPagin += `<input type="button" class=${i} value="${i}"/> `;
            $('#paginPanel').html(stringPagin);
        }
    }
    else {
        $('#paginPanel input').remove();
    }
    $('#paginPanel input:nth-child('+currentPage+')').css({'background-color':'green'});
    calcDoneNotDone();
}

function calcDoneNotDone() {
    var numberNotDone=0;
    var numberDone=0;
    todoList.forEach(function(item) {
        if (item.done == false) {
            numberNotDone+=1;
        }
        else {
            ++numberDone;
        }
    });
    document.getElementById("done").value=numberDone;
    document.getElementById("notdone").value=numberNotDone;
}

function editTodo() {
    $('.undertext').prop('readonly', false);
}
function deleteCompleted() {
    for (var i = todoList.length - 1; i >= 0; i--) {
        if (todoList[i].done == true) {
            todoList.splice(i, 1);

        }
        else {
        }
    }
    print(todoList);
}
function showAll() {
    mode = 'all';
    currentPage=1;
    $('#showAll').css({'background-color':'green'});
    $('#showOnlyNotDone').css({'background-color':'buttonface'});
    $('#showOnlyDone').css({'background-color':'buttonface'});
    print(todoList);

}

function showOnlyNotDone() {
    mode = 'notDone';
    currentPage=1;
    $('#showAll').css({'background-color':'buttonface'});
    $('#showOnlyNotDone').css({'background-color':'green'});
    $('#showOnlyDone').css({'background-color':'buttonface'});
    print(todoList);


}

function showOnlyDone() {
    mode = 'done';
    currentPage=1;
    $('#showAll').css({'background-color':'buttonface'});
    $('#showOnlyNotDone').css({'background-color':'buttonface'});
    $('#showOnlyDone').css({'background-color':'green'});
    print(todoList);


}

function filterByMode(list){
    if(mode==='all') {
        return list;
    }
    return _.where(list,{done:mode=='notDone'?false:true});
}